const user = {};

function setElListener() {
    console.log('fdsfsd')

    const gender = $('[name="gender"]');
    const activityLevel = $('[name="activity_level"]');
    const meal = $('[name="meal[]"]');
    const age = $('[name="age"]');
    const height = $('[name="height"]');
    const weight = $('[name="weight"]');
    const targetWeight = $('[name="target_weight"]');

    $(gender).on('click', function(el) {
        user.gender = $(el.target).val();
    })

    $(activityLevel).on('click', function(el) {
        user.activityLevel = $(el.target).val();
    })

    $(meal).on('click', function(el) {
        const mealType = $(el.target).attr('class');

        setTimeout(() => {
            switch (mealType) {
                case 'meat':
                    user.meatFood = [];
                    $('[name="meal[]"].meat:checked').each((i, meat) => {
                        user.meatFood.push($(meat).val());
                    });
                    break;

                case 'veggie':
                    user.veggieFood = [];
                    $('[name="meal[]"].veggie:checked').each((i, veggie) => {
                        user.veggieFood.push($(veggie).val());
                    });
                    break;

                case 'other_food':
                    user.otherFood = [];
                    $('[name="meal[]"].other_food:checked').each((i, other) => {
                        user.otherFood.push($(other).val());
                    });
            }
        }, 10)

    })

    $(age).on('input', function(el) {
        user.age = $(el.target).val();
    })

    $(height).on('input', function(el) {
        user.height = $(el.target).val();
    })

    $(weight).on('input', function(el) {
        user.weight = $(el.target).val();
    })

    $(targetWeight).on('input', function(el) {
        user.targetWeight = $(el.target).val();
    })
}


$('.keto-step-form-wrap h1').bind('DOMSubtreeModified', () => {
    setTimeout(() => {
        setElListener();
        console.clear();
        console.log(user);
    }, 50)

});

$(document).ready(function() {
    setElListener();
});


$('#formElem').on('submit', function(e) {
    e.preventDefault();

    $('[name="activityLevel"]').val(user.activityLevel);
    $('[name="gender"]').val(user.gender);
    $('[name="meatFood"]').val(JSON.stringify(user.meatFood));
    $('[name="veggieFood"]').val(JSON.stringify(user.veggieFood));
    $('[name="otherFood"]').val(JSON.stringify(user.otherFood));

    this.submit();
})
